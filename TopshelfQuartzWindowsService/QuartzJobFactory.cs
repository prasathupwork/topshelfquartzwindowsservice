﻿using LightInject;
using Quartz;
using Quartz.Spi;

namespace TopshelfQuartzWindowsService
{
    public class QuartzJobFactory : IJobFactory
    {
        IServiceContainer _container;
        public QuartzJobFactory(IServiceContainer container)
        {
            _container = container;
        }

        #region Implementation of IJobFactory

        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            return _container.GetInstance(bundle.JobDetail.JobType) as IJob;
        }

        public void ReturnJob(IJob job)
        {
           
        }

        #endregion
    }
}
