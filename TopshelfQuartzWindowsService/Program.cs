﻿using System;
using Topshelf;
using TopshelfQuartzWindowsService.Service;

namespace TopshelfQuartzWindowsService
{
    class Program
    {
        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += LogException;

            var container = LightInjectConfig.Build();

            HostFactory.Run(config =>
            {
                config.Service<IAutoScannerService>(service =>
                {
                    service.ConstructUsingLightInject(container);

                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });

                config.EnableServiceRecovery(r =>
                {
                    r.RestartService(1);
                    r.RestartService(10);
                    r.RestartService(30);
                    r.SetResetPeriod(0);
                });

                config.OnException(e =>
                {
                    //TODO
                });

                config.RunAsLocalService();

                config.SetServiceName("Payroll Document WQ Processor");
                config.SetDisplayName("Payroll Document WQ Processor");
                config.SetDescription("Payroll Document WQ Processor to queue messages in service bus");
            });
        }

        private static void LogException(object sender, UnhandledExceptionEventArgs e)
        {
            //TODO
        }
    }
}
