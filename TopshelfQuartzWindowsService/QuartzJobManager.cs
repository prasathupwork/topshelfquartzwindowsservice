﻿using Quartz;
using Quartz.Spi;

namespace TopshelfQuartzWindowsService
{
    public class QuartzJobManager
    {
        private readonly IJobFactory _jobFactory;
        private readonly ISchedulerFactory _schedulerFactory;

        public QuartzJobManager(ISchedulerFactory schedulerFactory, IJobFactory jobFactory)
        {
            _schedulerFactory = schedulerFactory;
            _jobFactory = jobFactory;
        }

        public IScheduler Add(IJobDetail jobDetail, ITrigger trigger)
        {
            IScheduler scheduler = _schedulerFactory.GetScheduler();
            scheduler.JobFactory = _jobFactory;
            scheduler.Start();
            scheduler.ScheduleJob(jobDetail, trigger);
            return scheduler;
        }
    }
}
