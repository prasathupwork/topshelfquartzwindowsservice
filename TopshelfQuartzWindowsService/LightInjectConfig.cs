﻿using LightInject;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using Topshelf.ServiceConfigurators;
using TopshelfQuartzWindowsService.Jobs;
using TopshelfQuartzWindowsService.Service;

namespace TopshelfQuartzWindowsService
{
    public static class LightInjectConfig
    {
        public static ServiceContainer Build()
        {
            var container = new ServiceContainer();
            container.Register<QuartzJobManager, QuartzJobManager>(new PerContainerLifetime());
            container.Register<IJobFactory, QuartzJobFactory>();
            container.Register<ISchedulerFactory, StdSchedulerFactory>();
            container.Register<IAutoScannerService, AutoScannerService>();

            container.Register<QueuePayrollReportsJob, QueuePayrollReportsJob>();
            container.Register<QueueTaxReportJob, QueueTaxReportJob>();

            container.RegisterInstance<IServiceContainer>(container);
            return container;
        }

        public static ServiceConfigurator<T> ConstructUsingLightInject<T>(this ServiceConfigurator<T> configurator, IServiceContainer container) where T : class
        {
            configurator.ConstructUsing(serviceFactory => container.GetInstance<T>());

            return configurator;
        }
    }
}
