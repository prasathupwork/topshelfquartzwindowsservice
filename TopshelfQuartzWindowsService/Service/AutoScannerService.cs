﻿using Quartz;
using System.Collections.Generic;
using TopshelfQuartzWindowsService.Jobs;

namespace TopshelfQuartzWindowsService.Service
{
    /// <summary>
    /// This class is implementing IAutoScannerService(start, stop) methods
    /// </summary>
    public class AutoScannerService : IAutoScannerService
    {
        private readonly QuartzJobManager _jobManager;
        private readonly List<IScheduler> _schedulers = new List<IScheduler>();

        public AutoScannerService(QuartzJobManager jobManager)
        {
            _jobManager = jobManager;
        }

        public void Start()
        {
            var payrollReportJob = JobBuilder.Create<QueuePayrollReportsJob>().WithIdentity("PayrollJob").Build();
            var payrollReportTrigger = TriggerBuilder.Create().WithSimpleSchedule(builder => builder.WithIntervalInSeconds(10).RepeatForever()).Build();
            var payrollReportScheduler = _jobManager.Add(payrollReportJob, payrollReportTrigger);
            _schedulers.Add(payrollReportScheduler);

            var taxReportJob = JobBuilder.Create<QueueTaxReportJob>().WithIdentity("TaxJob").Build();
            var taxReportTrigger = TriggerBuilder.Create().WithSimpleSchedule(builder => builder.WithIntervalInSeconds(10).RepeatForever()).Build();
            var taxReportScheduler = _jobManager.Add(taxReportJob, taxReportTrigger);
            _schedulers.Add(taxReportScheduler);
        }

        public void Stop()
        {
            _schedulers.ForEach(scheduler => scheduler.Shutdown(false));
            _schedulers.Clear();
        }
    }
}
