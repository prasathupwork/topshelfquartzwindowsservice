﻿using System;

namespace TopshelfQuartzWindowsService.Service
{
    public interface IAutoScannerService
    {
        void Start();
        void Stop();
    }
}
