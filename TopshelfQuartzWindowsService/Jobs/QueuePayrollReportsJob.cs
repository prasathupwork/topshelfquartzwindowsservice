﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TopshelfQuartzWindowsService.Jobs
{
    [DisallowConcurrentExecution]
    public class QueuePayrollReportsJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine(string.Format("{0} : Payroll Called", DateTime.Now));
            Thread.Sleep(8000);
            Console.WriteLine(string.Format("{0} : Completed", DateTime.Now));
        }
    }
}
