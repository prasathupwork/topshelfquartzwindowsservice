﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TopshelfQuartzWindowsService.Jobs
{
    public class QueueTaxReportJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Console.WriteLine(string.Format("{0} : Tax Called", DateTime.Now));
        }
    }
}
